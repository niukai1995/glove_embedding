import tensorflow as tf

# global unique layer ID dictionary for layer name assignment
_LAYER_UIDS = {}


def get_layer_uid(layer_name=''):
    """
    Helper function, assigns unique layer IDs.
        e.g. Dense_1 ; Dense_2 ...
    """
    if layer_name not in _LAYER_UIDS:
        _LAYER_UIDS[layer_name] = 1
        return 1
    else:
        _LAYER_UIDS[layer_name] += 1
        return _LAYER_UIDS[layer_name]


class Layer(object):
    """Base layer class. Defines basic API for all layer objects.
    Implementation inspired by keras (http://keras.io).

    # Properties
        name: String, defines the variable scope of the layer.
        
        logging: Boolean, switches Tensorflow histogram logging on/off

    # Methods
        _call(inputs): Defines computation graph of layer
            (i.e. takes input, returns output)
            
        __call__(inputs): Wrapper for _call()
        
        _log_vars(): Log all variables
    """
    def __init__(self, **kwargs):
        allowed_kwargs = {'name', 'logging'}
        for kwarg in kwargs.keys():
            assert kwarg in allowed_kwargs, 'Invalid keyword argument: ' + kwarg
            
        name = kwargs.get('name')
        
        if not name:
            layer = self.__class__.__name__.lower()
            name = layer + '_' + str(get_layer_uid(layer))
            
        self.name = name
        self.vars = {}
        
        logging = kwargs.get('logging', False)
        
        self.logging = logging
        self.sparse_inputs = False

    def _call(self, inputs):
        return inputs

    def __call__(self, inputs):
        with tf.name_scope(self.name):
            if self.logging and not self.sparse_inputs:
                tf.summary.histogram(self.name + '/inputs', inputs)
            
            outputs = self._call(inputs)
            
            if self.logging:
                tf.summary.histogram(self.name + '/outputs', outputs)
            
            return outputs
        
    def _log_vars(self):
        for var in self.vars:
            tf.summary.histogram(self.name + '/vars/' + var, self.vars[var])


class Glove(Layer):
    def __init__(self, vocab_size, embedding_dim, placeholders, **kwargs):
        '''
        vocab_size and embedding_dim are hyperparameters
        placeholders are used as parts of the computational graph
        '''
        super(Glove, self).__init__(**kwargs)
        
        # it's better to use uniform random generator than tf.random.normal
        self.vars['weights_1'] = tf.Variable(tf.random.uniform([vocab_size, embedding_dim],-0.5, 0.5), name='weights_1')
        self.vars['weights_2'] = tf.Variable(tf.random.uniform([vocab_size, embedding_dim],-0.5, 0.5), name='weights_2')
         
        self.vars['bias_1'] = tf.Variable(tf.zeros([vocab_size, 1]), name='bias_1')
        self.vars['bias_2'] = tf.Variable(tf.zeros([vocab_size, 1]), name='bias_2')


    def _call(self, inputs):
        '''
        inputs is a dictionary
            inputs['x1']: |V| * |batch|
                - each column is one hot for index of the word_i
            inputs['x1']: |V| * |batch|
                - each column is one hot for index of the word_j
            inputs['y']: |batch| * 1
                - each element is the value of X_{i,j}
            inputs['weights']: |batch| * 1
                - each elements is the value of f(X_{i,j})
            
        '''
        
        
        # batch * embedding_dim
        h1 = tf.nn.embedding_lookup(self.vars['weights_1'], inputs['x1'])
        h2 = tf.nn.embedding_lookup(self.vars['weights_2'], inputs['x2'])
         # batch * 1
        b1 =  tf.nn.embedding_lookup(self.vars['bias_1'], inputs['x1'])
        b2 = tf.nn.embedding_lookup(self.vars['bias_2'], inputs['x2'])
        
        dot_product_of_two_vectors = tf.reduce_sum(h1*h2, axis=1, keepdims=True)
        
        # 1 * batch
        output = tf.reshape(tf.math.add_n([dot_product_of_two_vectors, b1, b2]), (1,-1))

        return output
