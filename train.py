#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  1 10:18:06 2019

@author: Kai Niu
"""
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import mxnet as mx
import numpy as np
import math
from utils import get_batch_data,get_training_dev_dataset,load_obj,output_model
from model import Glove_Embedding
from evaluate import word_sim,word_analogy
from gensim.models import KeyedVectors

# Set random seed
seed = 123
np.random.seed(seed)
tf.set_random_seed(seed)

def construct_feed_dict(x1,x2, labels, placeholders):
    """Construct feed dictionary."""
    feed_dict = dict()
    
    feed_dict.update({placeholders['x1']: x1})
    feed_dict.update({placeholders['x2']: x2})
    feed_dict.update({placeholders['labels']: labels})
    
    return feed_dict

vocab = load_obj("vocab")
vocab_num = len(vocab)
id_to_word = {val:key for key,val in vocab.items()}

# Define placeholders
placeholders = {
     'x1': tf.placeholder(tf.int32, shape=(None)),
     'x2': tf.placeholder(tf.int32, shape=(None)), 
    'labels': tf.placeholder(tf.float32, shape=(1, None)),
}

# Create model
#placeholders, vocab_size, embedding_dim = 300, max_num = 100, alpha = 0.75, lr=0.01, **kwargs):
model = Glove_Embedding(placeholders, vocab_size = vocab_num,embedding_dim=300, 
                        lr=0.005, logging=True)

# 让TensorFlow按需分配显存。
session_conf = tf.ConfigProto(
          allow_soft_placement=True,
          log_device_placement=True)

session_conf.gpu_options.allow_growth = True
sess = tf.Session(config=session_conf)


writer = tf.summary.FileWriter('./graphs', tf.get_default_graph())

# Init variables
sess.run(tf.global_variables_initializer())

cost_val = []


best_dev_loss = math.inf

context = mx.cpu()

outs = []
print('starting training')
step = 0

epochs = 15
train_dataset, dev_dataset = get_training_dev_dataset('./cooccurrences_fre_15_window_10_min_count_1.csv')
for i in range(epochs):
    print('epochs',i,'.........................')
    bath_size = 3000
    term = 0

    BATH_SIZE = 4512
    for data in get_batch_data(train_dataset,BATH_SIZE):
        #data[]
        # convert pandas into ndarray
        data = data.values
    #    print(data)
        x1 = np.array(data[:,0],dtype=np.int32)#.reshape(1,-1)
        x2 = np.array(data[:,1],dtype=np.int32)#.reshape(1,-1)
        #print(x1.shape, x2.shape)
       
        label = np.array(data[:,-1]).reshape(1,-1)
        
        
        feed_dict = construct_feed_dict(x1, x2, label, placeholders)

        loss, _ = sess.run([model.loss, model.opt_op], feed_dict=feed_dict)
        outs.append(loss)
        # add log
        
        #writer.add_summary(loss_summ,step)
        if step % 3000 == 0:
            #print('step ',step, 'loss', test1, test2)
            print('step ',step, 'loss', sum(outs)/len(outs))
            outs = []    
        
       
        step += 1

    # development dataset
    dev_loss = 0
    for data in get_batch_data(dev_dataset,BATH_SIZE):
        data = data.values

    #    print(data)
        x1 = np.array(data[:,0],dtype=np.int32)#.reshape(1,-1)
        x2 = np.array(data[:,1],dtype=np.int32)#.reshape(1,-1)
        
        
    
        label = np.array(data[:,-1]).reshape(1,-1)
        
        feed_dict = construct_feed_dict(x1, x2, label, placeholders)
        loss = sess.run(model.loss, feed_dict=feed_dict)
        dev_loss += loss*len(data)
        
    current_dev_loss = dev_loss/len(dev_dataset)
    print('steps',step,'dev loss', current_dev_loss)
    if  current_dev_loss < best_dev_loss:
        best_dev_loss = current_dev_loss
        model.save(sess)

        # test dataset evaluation
        if i > 10:
            matrix1,matrix2 = sess.run([model.layers[0].vars['weights_1'],model.layers[0].vars['weights_2']])
            output_model(matrix1, matrix2, id_to_word, "./output_matrix-5")
            embeddings = KeyedVectors.load_word2vec_format('./output_matrix-5')
            print("Word Analogies")
            word_analogy(embeddings)
            print("Word Similarity")
            word_sim(embeddings)
    if i % 10 == 0:
        matrix1,matrix2 = sess.run([model.layers[0].vars['weights_1'],model.layers[0].vars['weights_2']])
        output_model(matrix1, matrix2, id_to_word, "./output_matrix-5")
        embeddings = KeyedVectors.load_word2vec_format('./output_matrix-5')
        print("Word Analogies")
        word_analogy(embeddings)
        print("Word Similarity")
        word_sim(embeddings)
           
writer.close()
