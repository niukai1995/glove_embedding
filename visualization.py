#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  1 14:20:51 2019

@author: Kai Niu
"""

import matplotlib.pyplot as plt
import matplotlib.cm as cm
from sklearn.manifold import TSNE
import numpy as np

# plot function
def tsne_plot_similar_words(title, labels, embedding_clusters, word_clusters, a, filename=None):
    plt.figure(figsize=(16, 9))
    colors = cm.rainbow(np.linspace(0, 1, len(labels)))
    for label, embeddings, words, color in zip(labels, embedding_clusters, word_clusters, colors):
        x = embeddings[:, 0]
        y = embeddings[:, 1]
        plt.scatter(x, y, c=color, alpha=a, label=label)
        for i, word in enumerate(words):
            plt.annotate(word, alpha=0.5, xy=(x[i], y[i]), xytext=(5, 2),
                         textcoords='offset points', ha='right', va='bottom', size=8)
    plt.legend(loc=4)
    plt.title(title)
    plt.grid(True)
    if filename:
        plt.savefig(filename, format='png', dpi=150, bbox_inches='tight')
    plt.show()

def plot(keys=['plane_bn:00062766n','plane_bn:00001697n', 'bank_bn:00008364n', 'bank_bn:00008363n'],N=10):
    '''
    Parameters：
        keys: a list containing all the word you want to show in the plot
        N: show top N similar words for each word in keys
    '''
    embedding_clusters = []
    word_clusters = []
    for word in keys:
        embeddings = []
        words = []
        for similar_word, _ in w2v_model.wv.most_similar(word, topn=N):
            words.append(similar_word)
            embeddings.append(w2v_model[similar_word])
        embedding_clusters.append(embeddings)
        word_clusters.append(words)
    
    
    embedding_clusters = np.array(embedding_clusters)
    n, m, k = embedding_clusters.shape
    tsne_model_en_2d = TSNE(perplexity=5, n_components=2, init='pca', n_iter=3500, random_state=32)
    embeddings_en_2d = np.array(tsne_model_en_2d.fit_transform(embedding_clusters.reshape(n * m, k))).reshape(n, m, 2)
    
    tsne_plot_similar_words('Similar words', ['plane_Mathematics','plane_transport','financial_bank','river_bank'], embeddings_en_2d, word_clusters, 0.7,
                        'plane_bank.png')
if __name__ == "__main__":
    plot()





