#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  1 09:58:56 2019

@author: Kai Niu
"""
from collections import Counter
import logging

import pandas as pd
import numpy as np
import csv
import pickle
from nltk.corpus import stopwords
from collections import defaultdict


logger = logging.getLogger("glove")
def save_obj(obj, name ):
    with open('obj/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def load_obj(name ):
    with open('obj/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)
    
def output_model(matrix1, matrix2, id_to_word, output_file_name):

    matrix = (matrix1 + matrix2)/2

    size = matrix.shape
    
    f_out = open(output_file_name,'w')
    
    f_out.write(str(size[0]) + ' ' + str(size[1])+'\n')
    
    for i in range(size[0]):
        word = id_to_word[i]
        embedding = [str(ele) for ele in matrix[i,:]]
        assert len(embedding) == size[1]
        
        data = word + ' ' + ' '.join(embedding)
        f_out.write(data + '\n')


def build_vocab(corpus,min_frequent=20):
    '''
    build_cooccur accepts a corpus and yields a list of 
    co-occurrence blobs (the Xij values). It calculates 
    co-occurrences by moving a sliding n-gram window over
    each sentence in the corpus.
    '''
    
    logger.info("Building vocab from corpus")
    
    vocab = Counter()
    for line in corpus:
        tokens = line.strip().split()
        vocab.update(tokens)
    
    logger.info("Done building vocab from corpus.")
    
    filter_vocab = []
    filter_vocab = [word  for i,(word, freq)  in enumerate(vocab.items()) if freq >= min_frequent]
    vocab_dict = {val:key+1 for key,val in enumerate(filter_vocab)}
    vocab_dict['UNK'] = 0
    
    # save the object
    save_obj(vocab_dict, "vocab")

    return vocab_dict
    

def build_cooccur(vocab, corpus, window_size=10, min_count=None):
    """
    Build a word co-occurrence list for the given corpus.


    If `min_count` is not `None`, cooccurrence pairs where either word
    occurs in the corpus fewer than `min_count` times are ignored.
    """

    stopword = set(stopwords.words('english'))

    # using builtin-data structure to store the count information
    cooccurrences = defaultdict(defaultdict)
    
    print('begin co-occurrence matrix building')
    for i, line in enumerate(corpus):
        if i % 300 == 0:
            print("Building cooccurrence matrix: on line %i", i)

        tokens = line.strip().split()
        
        # filter stop words
        tokens = [word for word in tokens if word not in stopword]
        
        # convert words into word_id 
        token_ids = [int(vocab[word]) if word in vocab else vocab['UNK'] for word in tokens ]
        
        for center_i, center_id in enumerate(token_ids):
            # Collect all word IDs in left window of center word
            context_ids = token_ids[max(0, center_i - window_size) : center_i]
            contexts_len = len(context_ids)

            for left_i, left_id in enumerate(context_ids):
                # Distance from center word
                
                # filter pairs containing 'UNK' words
                if center_id == vocab['UNK'] or left_id == vocab['UNK']:
                    continue
                distance = contexts_len - left_i

                # Weight by inverse of distance between words
                increment = 1.0 / abs(float(distance))

                # Build co-occurrence matrix symmetrically (pretend we
                # are calculating right contexts as well)
                if center_id not in cooccurrences or left_id not in cooccurrences[center_id]:
                    cooccurrences[center_id][left_id] = 0
                if left_id not in cooccurrences or center_id not in cooccurrences[left_id]:
                    cooccurrences[left_id][center_id] = 0

                cooccurrences[center_id][left_id] = increment + cooccurrences[center_id][left_id]
                cooccurrences[left_id][center_id] = increment + cooccurrences[left_id][center_id]

    # Now yield our tuple sequence (dig into the LiL-matrix internals to
    # quickly iterate through all nonzero cells)
    print('co-occurrence matrix building finished')
    
    # output the co-occurrence data
    print('starting store co-occurrence matrix')
    output_file = open('cooccurrences.csv', mode='w')
    output_writer = csv.writer(output_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    for i,val in cooccurrences.items():
        for j,count in val.items():
            if min_count and count <= min_count:
                continue
            output_writer.writerow([i, j, count])
        
                
    output_file.close()
    return  cooccurrences

def get_training_dev_dataset(path, dev_size = 8000):
    data = pd.read_csv(path,  dtype={0:np.int32,1:np.int32,2:np.float64}, header=None)
    data = data.sample(frac=1)
    
    train = data[dev_size:]
    dev = data[:dev_size]
    
    return train, dev

def get_batch_data(data, bath_size=30):
    #data = data.sample(frac=1)
    i = 1
    print('The size of training dataset', len(data))
    while i *  bath_size < len(data):
        yield data[(i-1)*bath_size: i*bath_size]
        i += 1

            
